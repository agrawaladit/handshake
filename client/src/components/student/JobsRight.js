import React, { Component } from 'react'
import { setApplications, getCompany } from '../UserFunctions'
import { Link, withRouter } from 'react-router-dom'
import Upload from '../Upload'
import { Row, Col } from 'react-bootstrap'

class JobsRight extends Component {

    state = {
        student_id: '',
        job_id: '',
        status: ''
    }

    static getDerivedStateFromProps(props, state) {
        return {
            student_id: props.sid,
            job_id: props.job._id,
            student_name: props.name,
            status: 'Pending'
        }
    }


    onClick = e => {
        setApplications(this.state).then(res => {
            console.log("Applied")
          })
    }



    render() {
        console.log(this.props.job)
        var job = this.props.job
        return (
            <div className="container-fluid pad-all">
                <Link to={`/profilec/${job.company._id}`} className="nav-link">
                    <h2 className="font-weight-bold" >{job.title}</h2>
                </Link>
                <h3 className="font-weight-normal">{job.company.company}</h3>
                <div className="row pad-all">
                    <p className="font-weight-light text-secondary mar-rt">{job.category}</p>
                    <p className="font-weight-light text-secondary mar-rt">{job.location}</p>
                    <p className="font-weight-light text-secondary mar-rt">{["Salary : " + job.salary + " $"]}</p>
                    <p className="font-weight-light text-secondary mar-rt">{["Posted : " + job.date]}</p>
                </div>
                <Row>
                <button type="button" class="btn btn-success mar-btm" onClick={this.onClick}>Apply</button>
                <Upload id={job._id} mode='image'></Upload>
                </Row>
                <br />
                <br />
                <h3 className="font-weight-bold">Job Description</h3>
                <p className="font-weight-light">{job.description}</p>

            </div>
        )
    }
}

export default withRouter(JobsRight)