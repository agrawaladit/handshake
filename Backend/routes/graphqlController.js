
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');
const express = require('express');
const router = express.Router();
const cors = require('cors')
const User = require('../models/User')
const Job = require('../models/Job')
const Company = require('../models/Company')
const Application = require('../models/Application')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
// const employeesService = require('../service/employeeService');

process.env.SECRET_KEY = 'secret'
router.use(cors())

// Construct a schema, using GraphQL schema language
const schema = buildSchema(`
  type Query {
    studentProfile(id: String!): User
    users: [User],
    getJobs : [Job],
    companyProfile(id: String!) : Company,
    getCompanies : [Company],
    getApplications : [Application]
  }

  type Mutation {
    studentLogin(email: String!, password: String!): String,
    studentRegister(first_name: String, last_name: String, email: String, password: String, school: String) : String,
    updateEducation(id: String!, location: String, degree: String, start_date: String, end_date: String, school: String, major: String, cgpa: String): String,
    updateExperience(id: String, company: String, title: String, description: String, location: String, start_date: String, end_date: String, duration: String): String,
    setJob(company: String, title: String, date: String, description: String, location: String, deadline: String, category: String, salary: String) : String,
    companyRegister(company: String, email: String, password: String, location: String): String,
    companyLogin(email: String!, password: String!): String,
    setApplication(student: String, job: String, student_name: String, status: String): String,
    updateApplicationStatus(id: String!, status: String!): String

  }

  type Application {
    status: String,
    job: String,
    student: String,
    student_name: String
  }

  type Company {
    company: String!,
    email: String!,
    password: String!,
    location: String,
    description: String,
    image: String
  }

  type User {
    first_name: String!
    last_name: String!
    email: String!
    password: String!
    school: String!
    contact: UserContact
    education: UserEducation
    experience: UserExperience
  }
  type UserContact {
    email: String,
    phone: String,
    image: String,
    resume: String
  }
  type UserEducation {
    school: String,
    major: String,
    location: String,
    start_date: String,
    end_date: String,
    degree: String,
    cgpa: String
  }
  type UserExperience {
    company: String,
    title: String,
    location: String,
    description: String,
    duration: String,
    start_date: String,
    end_date: String
  }

  type Job {
    title: String,
    date: String,
    deadline: String,
    location: String,
    salary: String,
    description: String,
    category: String,
    company: Company
  }
`);

const root = {
    studentProfile: (input) => {
        return new Promise((resolve, reject) => {
            User.findById(input.id, function (err, user) {
                if (err) reject(err);

                else if (user) {
                    resolve(user)
                } else {
                    resolve("User not found")
                }
            });
        })
    },

    companyProfile: (input) => {
        return new Promise((resolve, reject) => {
            Company.findById(input.id, function (err, company) {
                if (err) resolve(err);
            
                if (company) {
                    resolve(company)
                } else {
                    resolve('Company does not exist')
                }
              });
        })
    },

    users: () => {
        return new Promise((resolve, reject) => {
            User.find()
                .exec(function (err, users) {
                    if (err) reject(err)
                    resolve(users)
                })
        })
    },
    studentLogin: (input) => {
        return new Promise((resolve, reject) => {
            User.findOne({ email: input.email }, function (err, user) {
                if (err) reject(err);

                if (user) {
                    if (bcrypt.compareSync(input.password, user.password)) {
                        let token = jwt.sign(user.toJSON(), process.env.SECRET_KEY, {
                            expiresIn: 1440
                        })
                        resolve(token)
                    }
                } else {
                    resolve('User does not exist')
                }
            });
        })
    },
    studentRegister: (input) => {
        return new Promise((resolve, reject) => {
            User.findOne({ email: input.email }, function (err, user) {
                if (err) reject(err);

                if (!user) {
                    bcrypt.hash(input.password, 10, (err, hash) => {
                        userData.password = hash
                        const newUser = new User(userData)
                        newUser.save()
                            .then(user => {
                                console.log(user.email)
                                resolve(user.email + ' Registered!')
                            })
                            .catch(err => {
                                resolve('error: ' + err)
                            })
                    })
                } else {
                    resolve({ error: 'User already exists' })
                }
            });
        })
    },
    updateEducation: (input) => {
        return new Promise((resolve, reject) => {
            const educationData = {
                location: input.location,
                degree: input.degree,
                start_date: input.start_date,
                end_date: input.end_date,
                school: input.school,
                major: input.major,
                cgpa: input.cgpa,
              }
            
              User.findOneAndUpdate({ _id: input.id }, {$set:{education:educationData}}, { upsert: true }, function (err, doc) {
                if (err) return resolve(500, { error: err });
                return resolve('Succesfully saved.');
              });
        })
    },

    updateExperience: (input) => {
        return new Promise((resolve, reject) => {
            const experienceData = {
                company: input.company,
                title: input.title,
                description: input.description,
                location: input.location,
                start_date: input.start_date,
                end_date: input.end_date,
                duration: input.duration
              }
            
              User.findOneAndUpdate({ _id: input.id }, {$set:{experience:experienceData}}, { upsert: true }, function (err, doc) {
                if (err) return resolve({ error: err });
                return resolve('Succesfully saved.');
              });
        })
    },
    companyLogin: (input) => {
        return new Promise((resolve, reject) => {
            Company.findOne({ email: input.email }, function (err, company) {
                if (err) reject(err);
            
                if (company) {
                  if (bcrypt.compareSync(input.password, company.password)) {
                    let token = jwt.sign(company.toJSON(), process.env.SECRET_KEY, {
                      expiresIn: 1440
                    })
                    resolve(token)
                  }
                } else {
                    resolve({ error: 'Company does not exist' })
                }
              });
        })
    },
    companyRegister: (input) => {
        return new Promise((resolve, reject) => {
            Company.findOne({ email: input.email }, function (err, company) {
                if (err) reject(err);
            
                if (!company) {
                  bcrypt.hash(input.password, 10, (err, hash) => {
                    companyData.password = hash
                    Company.create(companyData)
                      .then(company => {
                        console.log(company.email)
                        resolve(company.email + 'Registered!')
                      })
                      .catch(err => {
                        resolve('error: ' + err)
                      })
                  })
                } else {
                  resolve({ error: 'Company already exists' })
                }
              });
        })
    },
    setJob: (input) => {
        return new Promise((resolve, reject) => {
            const newJob = new Job({ company: input.company,
                title: input.title,
                description: input.description,
                location: input.location,
                deadline: input.deadline,
                category: input.category,
                salary: input.salary})
            newJob.save()
              .then(job => {
                resolve(job.title + ' Registered!' )
              })
              .catch(err => {
                resolve('error: ' + err)
              })
        })
    },
    getJobs: () => {
        return new Promise((resolve, reject) => {
            Job.find()
            .populate('company')
            .exec(function(err, jobs){
                if (err) reject(err)
                resolve(jobs)
            })
        })
    },

    
    getApplications: () => {
        return new Promise((resolve, reject) => {
            Application.find()
            .exec(function(err, apps){
                if (err) resolve(err)
                resolve(apps)
            })
        })
    },

    getCompanies: () => {
        return new Promise((resolve, reject) => {
            Company.find()
            .then(u => {
                resolve(u)
            })
            .catch(error => resolve(error))
        })
    },

    setApplication: (input) => {
        return new Promise((resolve, reject) => {
            const appData = {
                student: (input.student),
                job: (input.job),
                student_name: input.student_name,
                status: input.status
            }

            Application.findOne({
                student: (input.student),
                job: (input.job),
            }, function (err, application) {
                if (err) resolve(err);
        
                if (!application) {
                    Application.create(appData)
                        .then(application => {
                            resolve('Applied Successfully!' )
                        })
                        .catch(err => {
                            resolve('error: ' + err)
                        })
                } else {
                    resolve('Job Already Applied')
                }
            });
        })
    },

    updateApplicationStatus: (input) => {
        return new Promise((resolve, reject) => {
            Application.findOneAndUpdate({ _id: input.id }, {$set:{status:input.status}}, { upsert: true }, function (err, doc) {
                if (err) return resolve(500, { error: err });
                return resolve('Succesfully updated.');
              });
        })
    },
};

const graphql = graphqlHTTP({
    schema,
    rootValue: root,
    graphiql: true,
});

module.exports = graphql;
